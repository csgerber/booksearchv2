package edu.uchicago.gerber.booksearchv2.AA_view.views;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import androidx.navigation.Navigation;
import edu.uchicago.gerber.booksearchv2.CC_model.cache.Cache;
import edu.uchicago.gerber.booksearchv2.R;
import edu.uchicago.gerber.booksearchv2.Constants;



public class DetailFragment extends Fragment {


    private Button btnReturnPrevious;
    private TextView authorName;
    private TextView title;
    private TextView year;
    private TextView description;
    private ImageView imgBook;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.detail_fragment, container, false);

        authorName = view.findViewById(R.id.txt_authorName);
        title = view.findViewById(R.id.txt_titleName);
        year = view.findViewById(R.id.txt_yearName);
        description = view.findViewById(R.id.txt_descName);

        authorName.setText(getArguments().getString(Constants.author));
        title.setText(getArguments().getString(Constants.title));
        year.setText(getArguments().getString(Constants.year));
        description.setText(getArguments().getString(Constants.desc));


        //set imageView
        imgBook = view.findViewById(R.id.img_book);


        btnReturnPrevious = view.findViewById(R.id.btnReturnPrevious);
        btnReturnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });

        String link = getArguments().getString(Constants.imageUrlLarge);
        Glide.with(getContext())
                .load(link)
                .into(imgBook);

        return view;
    }


}