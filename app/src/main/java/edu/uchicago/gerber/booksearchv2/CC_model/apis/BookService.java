package edu.uchicago.gerber.booksearchv2.CC_model.apis;

import edu.uchicago.gerber.booksearchv2.CC_model.models.VolumesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BookService {

    @GET("/books/v1/volumes")
    Call<VolumesResponse> searchVolumes(
      @Query("q") String query,
      @Query("maxResults") String maxResults
    );
}
