package edu.uchicago.gerber.booksearchv2.BB_viewmodel.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import edu.uchicago.gerber.booksearchv2.CC_model.models.VolumesResponse;
import edu.uchicago.gerber.booksearchv2.CC_model.repositories.BookRepository;

public class ResultsListViewModel extends AndroidViewModel {
    private BookRepository bookRepository;
    private MutableLiveData<VolumesResponse> volumesResponseLiveData;

    //this is called from the Fragment in this line
    //viewModel = ViewModelProviders.of(this).get(BookSearchViewModel.class);
    public ResultsListViewModel(@NonNull Application application) {
        super(application);
        volumesResponseLiveData = new MutableLiveData<>();
        bookRepository = new BookRepository(volumesResponseLiveData);

    }

    //perform search
    public void  searchVolumes(String keyword) {
        bookRepository.searchVolumes(keyword);
    }

    //getter
    public LiveData<VolumesResponse> getVolumesResponseLiveData() {
        return volumesResponseLiveData;
    }

    //effective setter from cached Serialized object
    public void refreshFromCache(VolumesResponse volumeResponse){
        volumesResponseLiveData.postValue(volumeResponse);
    }

}