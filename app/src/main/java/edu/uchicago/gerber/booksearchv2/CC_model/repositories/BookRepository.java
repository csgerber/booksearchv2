package edu.uchicago.gerber.booksearchv2.CC_model.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import edu.uchicago.gerber.booksearchv2.CC_model.apis.BookService;
import edu.uchicago.gerber.booksearchv2.CC_model.cache.Cache;
import edu.uchicago.gerber.booksearchv2.CC_model.models.VolumesResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookRepository {
    private static final String BOOK_SEARCH_SERVICE_BASE_URL = "https://www.googleapis.com/";

    private static final String MAX_RESULTS = "40";
    private final BookService bookSearchService;
    private final MutableLiveData<VolumesResponse> volumesResponseLiveData;

    //the constructor creates a reference to the liveData object with callbacks
    public BookRepository(MutableLiveData<VolumesResponse> volumesResponseLiveData) {

        //instantiate the liveDataObject
        this.volumesResponseLiveData = volumesResponseLiveData;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        bookSearchService = new retrofit2.Retrofit.Builder()
                .baseUrl(BOOK_SEARCH_SERVICE_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(BookService.class);

    }


    //this corresponds to the GET call from the api
    public void searchVolumes(String keyword) {
        bookSearchService.searchVolumes(keyword, MAX_RESULTS)
                .enqueue(new Callback<VolumesResponse>() {
                    @Override
                    public void onResponse(Call<VolumesResponse> call, Response<VolumesResponse> response) {
                        if (response.body() != null) {
                            Cache.getInstance().setKeyword(keyword);
                            Cache.getInstance().stampMe();
                            volumesResponseLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<VolumesResponse> call, Throwable t) {
                        volumesResponseLiveData.postValue(null);
                    }
                });
    }





}
