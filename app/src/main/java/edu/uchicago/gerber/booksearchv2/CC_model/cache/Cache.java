package edu.uchicago.gerber.booksearchv2.CC_model.cache;

import edu.uchicago.gerber.booksearchv2.CC_model.models.VolumesResponse;

//singleton cache
public class Cache {


    private String keyword;
    private long stamp;


    private static Cache cache;

    private Cache(){};

    public static Cache getInstance(){
        if (null == cache){
            cache = new Cache();
            return cache;
        } else {
            return  cache;
        }
    }


    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;

    }

    //used to invalidate the cache
    public void stampMe(){
        stamp = System.currentTimeMillis();
    }
    public long getStamp(){
        return stamp;
    }

}
