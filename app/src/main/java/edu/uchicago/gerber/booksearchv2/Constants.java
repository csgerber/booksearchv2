package edu.uchicago.gerber.booksearchv2;

public class Constants {

    //these constants are used as keys throughout the 3-tier architecture
    public static String author = "AUTHOR";
    public static String title = "TITLE";
    public static String year = "YEAR";
    public static String desc = "DESC";
    public static String imageUrlLarge = "IMAGE_LARGE";


    //for invalidating the cache
    public static long time_to_stale_millis = 30_000;

}
