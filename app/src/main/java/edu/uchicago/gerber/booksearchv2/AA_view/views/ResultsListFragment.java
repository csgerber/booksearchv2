package edu.uchicago.gerber.booksearchv2.AA_view.views;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import edu.uchicago.gerber.booksearchv2.BB_viewmodel.viewmodels.ResultsListViewModel;
import edu.uchicago.gerber.booksearchv2.CC_model.cache.Cache;
import edu.uchicago.gerber.booksearchv2.CC_model.models.Item;
import edu.uchicago.gerber.booksearchv2.CC_model.models.VolumesResponse;
import edu.uchicago.gerber.booksearchv2.Constants;
import edu.uchicago.gerber.booksearchv2.R;
import edu.uchicago.gerber.booksearchv2.AA_view.adapters.ResultsListAdapter;

public class ResultsListFragment extends Fragment implements ResultsListAdapter.AdapterCallback {
    private ResultsListViewModel viewModel;
    private ResultsListAdapter adapter;
    private ProgressBar progressBar;
    private String keyword;
    private EditText keywordEditText;

    //used to pass data to detail frag
    private String auth, title, year, description, imageUrlLarge;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //pass a ref to the Fragment which is an AdapterCallback
        adapter = new ResultsListAdapter(this);
        viewModel = ViewModelProviders.of(this).get(ResultsListViewModel.class);
        viewModel.getVolumesResponseLiveData().observe(this, new Observer<VolumesResponse>() {
            @Override
            public void onChanged(VolumesResponse volumesResponse) {
                if (volumesResponse != null) {
                    adapter.setItems(volumesResponse.getItems());
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
        });

        //get the root container-view and set behavior of child views
        View view = inflater.inflate(R.layout.fragment_results, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.fragment_booksearch_searchResultsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.INVISIBLE);


        keywordEditText = view.findViewById(R.id.fragment_booksearch_keyword);

        keywordEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    keyword = keywordEditText.getEditableText().toString();
                    performSearch(keyword);
                    return true;
                }
                return false;

            }
        });



        return view;
    }

    //contract method from AdapterCallback
    @Override
    public void onBookClick(List<Item> items, int position) {
        fireDetailsFragment(items, position);
    }

    //this method will collect the data of the item, put it into a bundle, and pass it to the details frag
    private void fireDetailsFragment(List<Item> items, int position) {

        try {
            //we may get null-pointers on some of these methods, so we surround with try-catch and just snack to alert the user
            auth = items.get(position).getVolumeInfo().getAuthors().get(0);
            title = items.get(position).getVolumeInfo().getTitle();
            year = items.get(position).getVolumeInfo().getPublishedDate();
            imageUrlLarge = items.get(position).getVolumeInfo().getImageLinks().getThumbnail();
            description = items.get(position).getVolumeInfo().getDescription();

            Bundle bundle = new Bundle();
            bundle.putString(Constants.author, auth);
            bundle.putString(Constants.title, title);
            bundle.putString(Constants.year, year);
            bundle.putString(Constants.imageUrlLarge, imageUrlLarge);
            bundle.putString(Constants.desc, description);

            Navigation.findNavController(getView()).navigate(R.id.action_resultsFragment_to_detailFragment, bundle);


        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(this.getView(), "Insufficient details", Snackbar.LENGTH_LONG)
                    .setAction("CLOSE", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //do nothing, just dismiss it
                        }
                    })
                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                    .show();
        }

    }

    public void performSearch(String keyword) {
        MainActivity.hideKeyboardFrom(getContext(), ResultsListFragment.this.getView());
        progressBar.setVisibility(View.VISIBLE);
        viewModel.searchVolumes(keyword);
    }

    @Override
    public void onResume() {
        super.onResume();
        //not the first time into this method
        if (null != Cache.getInstance().getKeyword()){
            //check if cache is stale
            long stamp = Cache.getInstance().getStamp();
            if (System.currentTimeMillis() -  Constants.time_to_stale_millis > stamp  ) {
                performSearch(Cache.getInstance().getKeyword());
            }
        }

    }



}
